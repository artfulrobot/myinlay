(() => {
  console.log("myinlay code running!");
  if (!window.bootMyInlay) {
    // This is the first time this *type* of Inlay has been encountered.
    // We need to define anything global here.

    /**
     * The inlay object has the following properties:
     * - initData object of data served along with the bundle script.
     * - publicID string for the inlay instance on the server. Nb. you may have
     *   multiple instances of that instance(!) on a web page.
     * - script   DOM node of the script tag that has caused us to be loaded,
     *            e.g. useful for positioning our UI after it, or extracting
     *            locally specified data- attributes.
     * - request(fetchParams)
     *            method providing fetch() wrapper for all Inlay-related
     *            requests. The URL is fixed, so you only provide the params
     *            object.
     */
    window.bootMyInlay = inlay => {

      console.log("My Inlay booting!!")

      let div = document.createElement('div');
      div.textContent = "Boo!";
      inlay.script.insertAdjacentElement('afterend', div);

      div.textContent = "The contact is called " + inlay.initData.person ;
      let btn = document.createElement('button');
      btn.textContent = "Add suffix to their name";
      inlay.script.insertAdjacentElement('afterend', btn);

      btn.addEventListener('click', e => {
        btn.textContent = "just a mo!";
        inlay.request({method: 'post', body: { wantsTo: 'add a suffix!' }})
        .then(r => {
          div.textContent = "The contact is NOW called " + r.newName;
          btn.textContent = "Add suffix to their name again!";
        });
      });

    };
  }
})();

