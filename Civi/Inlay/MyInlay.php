<?php
namespace Civi\Inlay;

use CRM_Myinlay_ExtensionUtil as E;
use Civi\Api4\Contact;

/**
 * My silly Inlay!
 */
class MyInlay extends Type {

  public static $typeName = 'My inlay';

  const CONTACT_ID = 186;

  public static $defaultConfig = [
    'suffix' => '',
  ];

  /**
   * What data does the Javascript need immediately?
   */
  public function getInitData() :array {
    return [
      'init'   => 'bootMyInlay',
      'suffix' => $this->config['suffix'],
      'person' => $this->getTheContact()['display_name'],
    ];
  }

  /**
   * The main do-ing method.
   */
  public function processRequest(\Civi\Inlay\ApiRequest $request) :array {

    if (($request->getBody()['wantsTo'] ?? '') === 'add a suffix!') {

      $contact = $this->getTheContact();

      // Our silly update:
      Contact::update(FALSE)
        ->addWhere('id', '=', $contact['id'])
        ->setValues([
          'last_name' => $contact['last_name'] . $this->config['suffix']
        ])
        ->execute();

      // reload display name
      $newName= $this->getTheContact()['display_name'];

      // Data to return to Javascript
      return ['newName' => $newName];
    }
    // Anything else is an error.
    return ['error' => 'unrecognised request' . json_encode($request->getBody())];
  }

  /**
   * Get the Javascript to export.
   */
  public function getExternalScript() :string {
    return file_get_contents(E::path('myinlay.js'));
  }

  /**
   * Internal helper to clean up the code.
   */
  protected function getTheContact(): array {
    return Contact::get(FALSE)
      ->addWhere('id', '=', self::CONTACT_ID)
      ->execute()->first();
  }

}
