<?php
// This file declares an Angular module which can be autoloaded
// in CiviCRM. See also:
// \https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules/n
return [
  'js' => [
    'ang/crmMyinlay.js',
    'ang/crmMyinlay/*.js',
    'ang/crmMyinlay/*/*.js',
  ],
  'css' => [
    'ang/crmMyinlay.css',
  ],
  'partials' => [
    'ang/crmMyinlay',
  ],
  'requires' => [
    'crmUi',
    'crmUtil',
    'ngRoute',
  ],
  'settings' => [],
];
