(function(angular, $, _) {
  console.log("xxxxxxxxxx");

  angular.module('crmMyinlay').config(function($routeProvider) {
      $routeProvider.when('/inlays/my_inlay/:id', {
        controller: 'CrmMyinlayMyInlay',
        controllerAs: '$ctrl',
        templateUrl: '~/crmMyinlay/MyInlay.html',

        // If you need to look up data when opening the page, list it out
        // under "resolve".
        resolve: {
          various: function($route, crmApi4) {
            const params = {
              inlayTypes: ['InlayType', 'get', {}, 'class'],
            };
            if ($route.current.params.id > 0) {
              params.inlay = ['Inlay', 'get', {where: [["id", "=", $route.current.params.id]]}, 0];
            }
            return crmApi4(params);
          }
        }
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  //   myContact -- The current contact, defined above in config().
  angular.module('crmMyinlay').controller('CrmMyinlayMyInlay', function($scope, crmApi4, crmStatus, crmUiHelp, various) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('myinlay');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/crmMyinlay/MyInlay'}); // See: templates/CRM/crmMyinlay/MyInlay.hlp
    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;

    $scope.inlayType = various.inlayTypes['Civi\\Inlay\\MyInlay'];
    if (various.inlay) {
      $scope.inlay = various.inlay;
    }
    else {
      $scope.inlay = {
        'class' : 'Civi\\Inlay\\MyInlay',
        name: 'New ' + $scope.inlayType.name,
        public_id: 'new',
        id: 0,
        config: JSON.parse(JSON.stringify($scope.inlayType.defaultConfig)),
      };
    }

    $scope.save = function() {
      return crmStatus(
        // Status messages. For defaults, just use "{}"
        {start: ts('Saving...'), success: ts('Saved')},
        crmApi4('Inlay', 'save', { records: [$scope.inlay] })
      ).then(r => {
        console.log("save result", r);
        window.location = CRM.url('civicrm/a?#inlays');
      });
    };

  });

})(angular, CRM.$, CRM._);
