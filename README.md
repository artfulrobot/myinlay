# myinlay

This inlay was created as a CiviCRM developer training aid.

What does it do?

- It will show a contact's name and provide a button to add a suffix to that contact's name.
- Pressing the button updates Civi and alters the UI to show the result.
- Provides back end config screen where you can set the suffix that gets added.
- Obviously this is NOT a useful thing other than for learning! Also note that the contact ID it uses is hard-coded (it exists in a buildkit with sample data environment).

This is only a bare minimum example.

After understanding this you might want to look at the 'simplest' (warning, bit of a misnomer) branch of inlaysignup: <https://github.com/artfulrobot/inlaysignup/tree/simplest> which provides popup "sign up for our newsletter" functionality, and demonstrates how you might want to use the CSRF tokenising to avoid spam submissions.